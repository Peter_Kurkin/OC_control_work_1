from datetime import datetime
import socket


def main():
    host = '127.0.0.1'
    port = 1303

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen(5)
        while True:
            conn, addr = s.accept()
            with conn:
                conn.sendall(str(datetime.now().strftime("%d.%m.%Y %H:%M")).encode())


if __name__ == '__main__':
    main()

