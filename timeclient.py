import socket


def main(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        print(s.recv(1024).decode(encoding='utf-8'))


if __name__ == '__main__':
    host = input("Введите ip адрес - ")
    port = 1303
    main(host, port)
